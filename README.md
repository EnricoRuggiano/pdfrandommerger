# SETUP

```
python3 -m pip install -i requirements.txt
```

# RUN

```
python3 main.py -i <input_dir> -o <output_dir> -t <threshold>
```