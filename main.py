#!/usr/bin/env python3
import argparse
from pdf_random import fetch_all_files, merge_pdf, random_extract_files

# setup parser
parser = argparse.ArgumentParser()
parser.add_argument('-o', '--out', 
                    required=True,
                    dest='out_path',
                    help='Provide destination output path file',
                    type=str
                    )

parser.add_argument('-i', '--input', 
                    required=True,
                    dest='dir',
                    help='Provide root input folder path',
                    type=str
                    )

parser.add_argument('-t', '--threshold', 
                    required=True,
                    dest='threshold',
                    help='Provide threshold for random extraction of the input files',
                    type=int
                    )

args = parser.parse_args()

# set input variables
threshold = args.threshold
dir       = args.dir
out_path  = args.out_path

all_files = fetch_all_files(dir)
extracted = random_extract_files(all_files, threshold=threshold)
merge_pdf(out_path, extracted)

print('File {} created succesfully '.format(out_path))