import os

def fetch_all_files(dir):
    target_files = []
    for path, subdirs, files in os.walk(dir):
        for name in files:
            target_files.append(os.path.join(path, name))
    return target_files 