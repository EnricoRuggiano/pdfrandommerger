import random

def random_extract_files(files, threshold=10):
    extracted_files = []
    while len(extracted_files) < threshold:
        if len(files) == 0:
            break
    
        i = random.randrange(0, len(files), 1)
        f = files[i]
        
        extracted_files.append(f)
        del files[i]
    return extracted_files